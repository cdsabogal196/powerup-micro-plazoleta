package com.pragma.powerup.usermicroservice.adapters.driving.http.controller;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.DishEntity;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.DishRequestDTO;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.IDishHandler;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.impl.DishHandlerImpl;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

class DishRestControllerTest {
    @InjectMocks
    private DishRestController dishRestController;


    @Mock
    private IDishHandler platoHandlerImpl;


    @BeforeEach
    void setUp() {

        platoHandlerImpl = mock(DishHandlerImpl.class);
        dishRestController = new DishRestController(platoHandlerImpl);
    }


    @Test
    public void testSavePlato() throws Exception {

        DishRequestDTO dishRequestDTO = new DishRequestDTO("", 10, "", "", "", true, 1);

        doNothing().when(platoHandlerImpl).saveDish("", "", dishRequestDTO);

        ResponseEntity<String> response = dishRestController.saveDish("", "", dishRequestDTO);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals("Dish saved successfully", response.getBody());
    }

    @Test
    public void testActivatePlatos() {
        Long id = 1L;
        ResponseEntity<String> response = dishRestController.activateDish(id, "", "");

        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);

    }

    @Test
    void testGetPlatosPorCategoria() throws Exception {

        String restauranteId = "1";
        String mail = "test@example.com";
        String password = "password";
        List<DishEntity> platosMock = Arrays.asList(new DishEntity(), new DishEntity());

        when(platoHandlerImpl.getDishByCategory(anyString(), anyString(), anyString()))
                .thenReturn(platosMock);

        ResponseEntity<List<DishEntity>> response = dishRestController.getDishByCategory(restauranteId, mail, password);

        assertEquals(platosMock, response.getBody());
        assertEquals(200, response.getStatusCodeValue());
    }

}