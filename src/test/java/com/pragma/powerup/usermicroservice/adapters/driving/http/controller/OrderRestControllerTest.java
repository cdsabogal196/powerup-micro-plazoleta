package com.pragma.powerup.usermicroservice.adapters.driving.http.controller;


import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.OrderEntity;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.OrderRequestDTO;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.IOrderHandler;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import static org.mockito.Mockito.verify;

import static com.google.common.base.Verify.verify;
import static org.mockito.Mockito.*;

class OrderRestControllerTest {

    @Mock
    private IOrderHandler pedidoHandler;

    @InjectMocks
    private OrderRestController orderRestController;
    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }



    @Test
    void testPlaceOrder() {
        OrderRequestDTO pedidoDTO = new OrderRequestDTO(1L, new Date(), "preferente", 2L, 3L, new ArrayList<>());
        pedidoDTO.setIdCliente(1L);
        pedidoDTO.setFecha(new Date());
        pedidoDTO.setEstado("Pending");
        pedidoDTO.setIdChef(2L);
        pedidoDTO.setIdRestaurante(3L);
        pedidoDTO.setPlatos(new ArrayList<>());
        String mail = "test@example.com";
        String password = "password";

        ResponseEntity<String> response = orderRestController.placeOrder(pedidoDTO, mail, password);

        assertEquals("The order was created successfully and is in state Pending", response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void testAssignOrder() {
        int idPedido = 1;
        OrderRequestDTO estadoPedidoDTO = new OrderRequestDTO(1L, new Date(), "preferente", 2L, 3L, new ArrayList<>());
        estadoPedidoDTO.setIdCliente(1L);
        estadoPedidoDTO.setFecha(new Date());
        estadoPedidoDTO.setEstado("Shipped");
        estadoPedidoDTO.setIdChef(2L);
        estadoPedidoDTO.setIdRestaurante(3L);
        estadoPedidoDTO.setPlatos(new ArrayList<>());
        String mail = "test@example.com";
        String password = "password";

        ResponseEntity<String> response = orderRestController.assignOrder(idPedido, mail, password, "");

        assertEquals("Order status has been updated to 'Preparation'.", response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void testGetOrdersByStatus() throws Exception {

        String estado = "Pending";
        int elementosPorPagina = 10;
        int numeroPagina = 1;
        String mail = "test@example.com";
        String password = "password";

        Page<OrderEntity> pedidosMock = mock(Page.class);
        when(pedidoHandler.getOrdersByStatus(estado, elementosPorPagina, numeroPagina, mail, password))
                .thenReturn(pedidosMock);

        ResponseEntity<Page<OrderEntity>> response = orderRestController.getOrdersByStatus(estado, elementosPorPagina, numeroPagina, mail, password);

        assertEquals(pedidosMock, response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testNotifyOrderReady() throws Exception {
        String idPedido = "123";
        String mail = "test@example.com";
        String password = "password";
        String expectedResponse = "Pedido listo";

        when(pedidoHandler.notifyOrderReady(idPedido, mail, password)).thenReturn(expectedResponse);

        ResponseEntity<String> response = orderRestController.notifyOrderReady(idPedido, mail, password);

        assertEquals(expectedResponse, response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testMarkOrderDelivered() {
        String idPedido = "1";
        String pinSeguridad = "1234";
        String mail = "mail@example.com";
        String password = "password";
        String idEmpleado = "";
        String correoEmpleado = "";

        orderRestController.markOrderDelivered(idPedido, pinSeguridad, mail, password, idEmpleado);

        verify(pedidoHandler).markOrderDelivered(idPedido, pinSeguridad, mail, password, idEmpleado);
    }

    @Test
    public void testCancelOrder() {
        Long idPedido = 1L;
        String mail = "mail@example.com";
        String password = "password";

        orderRestController.cancelOrder(idPedido, mail, password);

        verify(pedidoHandler).cancelOrder(idPedido, mail, password);
    }

}