package com.pragma.powerup.usermicroservice.adapters.driving.http.controller;

import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.RestaurantRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.RestaurantUser;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.RestaurantResponseDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.IRestaurantHandler;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.impl.RestaurantHandlerImpl;
import com.pragma.powerup.usermicroservice.configuration.Constants;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class RestaurantRestControllerTest {
    @InjectMocks
    private RestaurantRestController restaurantRestController;

    @Mock
    private IRestaurantHandler restaurantHandler;



    @BeforeEach
    void setUp() {
        restaurantHandler = mock(RestaurantHandlerImpl.class);
        restaurantRestController = new RestaurantRestController(restaurantHandler);
    }

    @Test
    public void testSaveRestaurant() throws Exception {
        RestaurantRequestDto restaurantRequestDto = new RestaurantRequestDto("2", "321", "", "", "", 1);

        ResponseEntity<Map<String, String>> response = restaurantRestController.saveRestaurant("","",restaurantRequestDto);

        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(Constants.RESTAURANT_CREATED_MESSAGE, response.getBody().get(Constants.RESPONSE_MESSAGE_KEY));
    }

    @Test
    public void testGetRestaurantData() throws Exception {
        String mail = "example@example.com";
        String password = "password123";
        RestaurantUser restaurantUser = new RestaurantUser(mail, password);

        List<RestaurantResponseDto> mockRestaurantData = new ArrayList<>();
        mockRestaurantData.add(new RestaurantResponseDto());
        mockRestaurantData.add(new RestaurantResponseDto());
        when(restaurantHandler.getRestaurantData(mail, password))
                .thenReturn(mockRestaurantData);

        ResponseEntity<List<RestaurantResponseDto>> response = restaurantRestController.getRestaurantData(restaurantUser);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(mockRestaurantData, response.getBody());
    }
}