package com.pragma.powerup.usermicroservice.adapters.driving.http.controller;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.OrderEntity;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.OrderRequestDTO;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.IOrderHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/pedido")
@RequiredArgsConstructor
public class OrderRestController {
    @Autowired
    private IOrderHandler pedidoHandler;

    @PostMapping
    public ResponseEntity<String> placeOrder(@RequestBody OrderRequestDTO pedidoDTO, String mail, String password) {
        try {
            pedidoHandler.placeOrder(pedidoDTO, mail, password);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return ResponseEntity.ok("The order was created successfully and is in state Pending");
    }

    @PutMapping("/cambiarEstadoPreparacion")
    public ResponseEntity<String> assignOrder(@RequestParam int idPedido, @RequestParam String mail,
                                              @RequestParam String password, @RequestParam String idEmpleado) {
        try {
            pedidoHandler.assignOrder(idPedido, mail, password, idEmpleado);
            return ResponseEntity.ok("El estado del pedido ha sido actualizado a 'Preparacion'.");
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @GetMapping
    public ResponseEntity<Page<OrderEntity>> getOrdersByStatus(@RequestParam("estado") String estado,
                                                               @RequestParam("elementosPorPagina") int elementosPorPagina, @RequestParam("numeroPagina") int numeroPagina,
                                                               String mail, String password) throws Exception {
        Page<OrderEntity> pedidos = pedidoHandler.getOrdersByStatus(estado, elementosPorPagina, numeroPagina,
                mail, password);
        return ResponseEntity.ok(pedidos);
    }

    @ApiIgnore
    @GetMapping("/notificarPedidoListo")
    public ResponseEntity<String> notifyOrderReady(@RequestParam("idPedido") String idPedido,
                                                   @RequestParam("mail") String mail, @RequestParam("password") String password) throws Exception {
        String response = pedidoHandler.notifyOrderReady(idPedido, mail, password);
        return ResponseEntity.ok(response);
    }

    @PostMapping("/{idPedido}/entregar/{pinSeguridad}")
    public void markOrderDelivered(@PathVariable String idPedido, @PathVariable String pinSeguridad, String mail,
                                   String password, String idEmpleado) {
        pedidoHandler.markOrderDelivered(idPedido, pinSeguridad, mail, password, idEmpleado);
    }

    @PostMapping("/{idPedido}/cancelar")
    public void cancelOrder(@PathVariable Long idPedido, String mail, String password) {
        pedidoHandler.cancelOrder(idPedido, mail, password);
    }
}
