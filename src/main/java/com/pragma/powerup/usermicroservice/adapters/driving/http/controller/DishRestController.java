package com.pragma.powerup.usermicroservice.adapters.driving.http.controller;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.DishEntity;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.DetailsDishRequestDTO;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.DishRequestDTO;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.IDishHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/plato")
@RequiredArgsConstructor
public class DishRestController {

    private final IDishHandler iDishHandler;

    @PostMapping("/save")
    public ResponseEntity<String> saveDish(String mail, String password, @RequestBody DishRequestDTO dishRequestDto) {
        try {
            iDishHandler.saveDish(mail, password, dishRequestDto);
            return ResponseEntity.ok("Dish saved successfully");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<String> updateDish(String mail, String password, @PathVariable("id") Long id, @RequestBody DetailsDishRequestDTO idDishRequestDTO) {
        try {
            iDishHandler.updateDish(id, mail, password, idDishRequestDTO.getPrecio(), idDishRequestDTO.getDescripcion());
            return ResponseEntity.ok("Plato actualizado exitosamente");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @PostMapping("/activatePlatos/{id}")
    public ResponseEntity<String> activateDish(@PathVariable("id") Long id, String mail, String password) {
        try {
            iDishHandler.activateDish(id, mail, password);
            return ResponseEntity.ok("Plato actualizado exitosamente");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @GetMapping("/platos/{restauranteId}/categoria")
    public ResponseEntity<List<DishEntity>> getDishByCategory(@PathVariable String restauranteId,
                                                              @RequestParam String mail,
                                                              @RequestParam String password) throws Exception {
        List<DishEntity> platos = iDishHandler.getDishByCategory(restauranteId, mail, password);
        return ResponseEntity.ok(platos);
    }

}

