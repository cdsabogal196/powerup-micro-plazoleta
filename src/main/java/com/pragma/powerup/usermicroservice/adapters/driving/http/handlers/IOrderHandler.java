
package com.pragma.powerup.usermicroservice.adapters.driving.http.handlers;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.OrderEntity;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.OrderRequestDTO;
import org.springframework.data.domain.Page;


public interface IOrderHandler {
    void placeOrder(OrderRequestDTO orderDTO, String mail, String password) throws Exception;

    void assignOrder(int idOrder, String mail, String password, String idEmployee) throws Exception;

    Page<OrderEntity> getOrdersByStatus(String state, int elementsByPage, int numberPage, String mail, String password) throws Exception;

    String notifyOrderReady(String idOrder, String mail, String password) throws Exception;

    void markOrderDelivered(String idOrder, String securityPin, String mail, String password, String idEmployee);

    void cancelOrder(Long idOrder, String mail, String password);
}
