package com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.impl;

import com.nimbusds.jose.shaded.gson.Gson;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.OrderEntity;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.OrderDishEntity;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.DishEntity;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.RestaurantEntity;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.OrderClientRequestDTO;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.OrderRequestDTO;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.JwtResponseDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.IOrderHandler;
import com.pragma.powerup.usermicroservice.domain.model.OrderStateEnum;
import com.pragma.powerup.usermicroservice.domain.model.RoleEnum;
import com.pragma.powerup.usermicroservice.domain.repositories.IPedidoPlatoRepository;
import com.pragma.powerup.usermicroservice.domain.repositories.IOrderRepository;
import com.pragma.powerup.usermicroservice.domain.repositories.IDishRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.*;

@Component
@Service
@RequiredArgsConstructor
public class OrderHandlerImpl implements IOrderHandler {
    @Autowired
    private IOrderRepository pedidoRepository;
    @Autowired
    private IPedidoPlatoRepository pedidoPlatoRepository;
    @Autowired
    private IDishRepository platoRepository;

    private HttpClient client = HttpClient.newHttpClient();

    public void generateTraceability(Long idOrder, String idClient, String mailClient, String previousState, String newState, Long idEmployee, String mailEmployee) {
        String requestBody = "{\"idOrder\": " + idOrder + ", \"idClient\": \"" + idClient + "\", \"mailClient\": \"" + mailClient + "\", \"previousState\": \"" + previousState + "\", \"newState\": \"" + newState + "\", \"idEmployee\": " + idEmployee + ", \"mailEmployee\": \"" + mailEmployee + "\"}";
        String endpoint = "http://localhost:8091/traceability";

        sendHttpRequest(requestBody, endpoint);
    }

    public String getRole(String mail, String password) {
        String requestBody = "{\"mail\":\"" + mail + "\", \"password\":\"" + password + "\"}";
        String endpoint = "http://localhost:8080/auth/login";

        String responseBody = sendHttpRequest(requestBody, endpoint);

        Gson gson = new Gson();
        JwtResponseDto responseObject = gson.fromJson(responseBody, JwtResponseDto.class);
        String token = responseObject.getToken();

        String[] chunks = token.split("\\.");
        Base64.Decoder decoder = Base64.getUrlDecoder();

        String payload = new String(decoder.decode(chunks[1]));
        JwtResponseDto jwtResponseDto = gson.fromJson(payload, JwtResponseDto.class);

        return jwtResponseDto.getRoles().get(0);

    }

    private String sendHttpRequest(String requestBody, String endpoint) {
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(endpoint))
                .header("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(requestBody))
                .build();

        HttpResponse<String> response;
        try {
            response = client.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }

        return response.body();
    }

    public String getIdEmployee(String mail) {

        HttpClient httpClient = HttpClient.newHttpClient();

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://localhost:8080/user/" + mail + "/id"))
                .GET()
                .build();

        HttpResponse<String> ownerResponse;
        try {
            ownerResponse = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return ownerResponse.body();
    }

    public void placeOrder(OrderRequestDTO orderDTO, String mail, String password) throws Exception {
        String role = getRole(mail, password);

        if (!role.equals(RoleEnum.ROLE_CLIENT.toString())) {
            throw new Exception("the user is not an client try again");

        }
        Random random = new Random();


        int randomNumber = random.nextInt(9000) + 1000;
        OrderEntity pedido = new OrderEntity();
        pedido.setIdCliente(orderDTO.getIdCliente());
        pedido.setFecha(new Date());
        pedido.setEstado(OrderStateEnum.Pendiente.toString());
        pedido.setIdChef(orderDTO.getIdChef());
        RestaurantEntity restaurant = new RestaurantEntity();
        restaurant.setId(orderDTO.getIdRestaurante());
        pedido.setRestaurante(restaurant);
        pedido.setPing(String.valueOf(randomNumber));

        pedido = pedidoRepository.save(pedido);

        List<OrderClientRequestDTO> platos = orderDTO.getPlatos();
        for (OrderClientRequestDTO pedidoCliente : platos) {
            OrderDishEntity orderDish = new OrderDishEntity();
            DishEntity plato = platoRepository.findById(String.valueOf(pedidoCliente.getIdPlato()));
            if (!plato.isActivo()) {
                throw new Exception("The dish wit the id: " + pedidoCliente.getIdPlato() + " is not active");
            }

            orderDish.setPedido(pedido);
            orderDish.setPlato(plato);
            orderDish.setCantidad(pedidoCliente.getCantidad());
            generateTraceability(orderDish.getPedido().getId(), String.valueOf(orderDTO.getIdCliente()), null, OrderStateEnum.Pendiente.toString(), OrderStateEnum.Pendiente.toString(), null, null);
            pedidoPlatoRepository.save(orderDish);
        }
    }

    public Page<OrderEntity> getOrdersByStatus(String state, int elementsByPage, int numberPage, String mail, String password) throws Exception {
        try {
            String role = getRole(mail, password);
            if (!role.equals(RoleEnum.ROLE_EMPLOYEE.toString())) {
                throw new Exception("The user is not an employee, please try again with other user.");
            }

            Pageable pageable = PageRequest.of(numberPage, elementsByPage);
            return pedidoRepository.findByEstado(state, pageable);
        } catch (Exception e) {
            throw new Exception("the info of the search didn't found");
        }
    }

    @Transactional
    public void assignOrder(int idOrder, String mail, String password, String idEmployee) throws Exception {
        String orderState = pedidoRepository.getEstadoById(String.valueOf(idOrder));
        OrderEntity order = pedidoRepository.findById(idOrder);
        String role = getRole(mail, password);

        if (!role.equals(RoleEnum.ROLE_EMPLOYEE.toString())) {
            throw new Exception("The user is not an employee. Please try with another user.");
        }
        if (orderState.equals(OrderStateEnum.Preparacion.toString())) {
            throw new Exception("The order is already on preparation.");
        }
        try {
            generateTraceability(order.getId(), order.getIdCliente().toString(), null, orderState, OrderStateEnum.Preparacion.toString(), Long.valueOf(idEmployee), mail);
        } catch (Exception e) {
            throw new Exception("Something was wrong with traceability microservice, details: " + e.getMessage());
        }

        pedidoRepository.changeOrderPreparation((long) idOrder);

    }

    @Transactional
    public String notifyOrderReady(String idOrder, String mail, String Password) throws Exception {
        String idClient = pedidoRepository.findIdClienteById(Long.valueOf(idOrder));
        HttpClient httpClient = HttpClient.newHttpClient();

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://localhost:8080/user/phone/" + idClient))
                .GET()
                .build();

        HttpResponse<String> ownerResponse;
        try {
            ownerResponse = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (Exception e) {
            e.printStackTrace();
            return idClient;
        }
        String phone = ownerResponse.body();

        pedidoRepository.notifyOrderReady(Long.valueOf(idOrder));
        String orderState = pedidoRepository.getEstadoById(idOrder);
        String pin = pedidoRepository.findPingById(Long.valueOf(idOrder));

        if (orderState.equalsIgnoreCase(OrderStateEnum.Listo.toString())) {
            String role = getRole(mail, Password);

            if (!role.equals(RoleEnum.ROLE_EMPLOYEE.toString())) {
                throw new RuntimeException("Unauthorized access. This role can't perform this action.");
            }

        } else {
            throw new RuntimeException("The order is not ready");
        }

        try {
            String idEmployee = getIdEmployee(mail);
            generateTraceability(Long.valueOf(idOrder), idClient, null, OrderStateEnum.Preparacion.toString(), OrderStateEnum.Listo.toString(), Long.valueOf(idEmployee), mail);
        } catch (Exception e) {
            throw new Exception("Something was wrong with traceability microservice, details: " + e.getMessage());
        }

        return phone + "/" + pin;
    }

    @Transactional
    public void markOrderDelivered(String idOrder, String securityPin, String mail, String Password, String idEmployee) {
        String orderState = pedidoRepository.getEstadoById(idOrder);
        OrderEntity order = pedidoRepository.findById(Long.parseLong(idOrder));
        String role = getRole(mail, Password);

        if (!role.equals(RoleEnum.ROLE_EMPLOYEE.toString())) {
            throw new RuntimeException("Unauthorized access. This role can't perform this action.");

        }

        if (orderState != null && orderState.equalsIgnoreCase(OrderStateEnum.Listo.toString())) {
            String pingCliente = pedidoRepository.findPingById(Long.valueOf(idOrder));

            if (securityPin.equals(pingCliente)) {

                try {
                    generateTraceability(Long.valueOf(idOrder), String.valueOf(order.getIdCliente()), null, orderState, OrderStateEnum.Entregado.toString(), Long.valueOf(idEmployee), mail);

                } catch (Exception e) {

                }
                pedidoRepository.markOrderDelivered(Long.valueOf(idOrder));
                System.out.println("Order marked as delivered.");
            } else {
                throw new RuntimeException("incorrect security pin.");
            }
        } else {
            throw new RuntimeException("The order is not in the 'Listo' state.");
        }
    }

    @Transactional
    public void cancelOrder(Long idOrder, String mail, String password) {
        String stateRequest = pedidoRepository.getEstadoById(String.valueOf(idOrder));
        OrderEntity order = pedidoRepository.findById((long) idOrder);
        String role = getRole(mail, password);

        if (!role.equals(RoleEnum.ROLE_CLIENT.toString())) {
            throw new RuntimeException("Unauthorized access. This role cannot perform this action.");
        }

        if (stateRequest != null && stateRequest.equalsIgnoreCase(OrderStateEnum.Pendiente.toString())) {
            try {
                generateTraceability(order.getId(), String.valueOf(order.getIdCliente()), null, stateRequest, OrderStateEnum.Cancelado.toString(), null, null);
            } catch (Exception e) {

            }

            pedidoRepository.cancelOrder(idOrder);

        } else {
            throw new RuntimeException("Sorry, your order is already being prepared and cannot be canceled.");
        }
    }


}

