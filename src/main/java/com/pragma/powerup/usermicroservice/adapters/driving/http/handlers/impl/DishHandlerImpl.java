package com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.impl;

import com.nimbusds.jose.shaded.gson.Gson;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.DishEntity;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.RestaurantEntity;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.DishRequestDTO;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.JwtResponseDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.IDishHandler;
import com.pragma.powerup.usermicroservice.domain.model.RoleEnum;
import com.pragma.powerup.usermicroservice.domain.repositories.IDishRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Base64;
import java.util.List;
import java.util.Optional;


@Service
@RequiredArgsConstructor
public class DishHandlerImpl implements IDishHandler {

    private final IDishRepository dishRepository;

    public String getRole(String mail, String password) {
        HttpClient client = HttpClient.newHttpClient();

        String requestBody = "{\"mail\":\"" + mail + "\", \"password\":\"" + password + "\"}";

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://localhost:8080/auth/login"))
                .header("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(requestBody))
                .build();

        HttpResponse<String> response;
        try {
            response = client.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }

        String responseBody = response.body();

        Gson gson = new Gson();
        JwtResponseDto responseObject = gson.fromJson(responseBody, JwtResponseDto.class);
        String token = responseObject.getToken();

        String[] chunks = token.split("\\.");
        Base64.Decoder decoder = Base64.getUrlDecoder();
        String payload = new String(decoder.decode(chunks[1]));
        JwtResponseDto jwtResponseDto = gson.fromJson(payload, JwtResponseDto.class);

        return jwtResponseDto.getRoles().get(0);
    }

    public void saveDish(String mail, String password, DishRequestDTO dishRequestDto) throws Exception {
        String role = getRole(mail, password);

        if (role.equals(RoleEnum.ROLE_OWNER.toString())) {
            RestaurantEntity restaurante = new RestaurantEntity((long) dishRequestDto.getIdRestaurante(), "", "", "", "", "", 0);

            DishEntity dishEntity = new DishEntity();
            dishEntity.setNombre(dishRequestDto.getNombre());
            dishEntity.setPrecio(dishRequestDto.getPrecio());
            dishEntity.setDescripcion(dishRequestDto.getDescripcion());
            dishEntity.setUrlImagen(dishRequestDto.getUrlImagen());
            dishEntity.setCategoria(dishRequestDto.getCategoria());
            dishEntity.setActivo(dishRequestDto.isActivo());
            dishEntity.setRestaurante(restaurante);

            dishRepository.save(dishEntity);
        } else {
            throw new Exception("The dishes cannot be created");
        }
    }

    @Override
    public void updateDish(Long id, String mail, String password, int price, String description) {
        String role = getRole(mail, password);

        if (role.equals(RoleEnum.ROLE_OWNER.toString())) {
            DishEntity plato = dishRepository.findById(id.toString());
            if (plato != null) {
                plato.setPrecio(price);
                plato.setDescripcion(description);
                dishRepository.save(plato);
            } else {
                throw new IllegalArgumentException("Dish not found with the provided ID: " + id);
            }
        }
    }

    @Override
    public void activateDish(Long id, String mail, String password) {
        String role = getRole(mail, password);

        if (role.equals(RoleEnum.ROLE_OWNER.toString())) {
            Optional<DishEntity> platoOptional = dishRepository.findById(id);

            if (platoOptional.isPresent()) {
                DishEntity plato = platoOptional.get();
                if (plato.isActivo()) {
                    plato.setActivo(false);
                } else {
                    plato.setActivo(true);
                }
                dishRepository.save(plato);
                System.out.println("Row from table 'plato' with id " + id + " checked.");
            } else {
                System.out.println("Could not find row of table 'plate' with id " + id + ".");
            }
        } else {
            try {
                throw new Exception("This role is unathorized to perform this action");
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public List<DishEntity> getDishByCategory(String IdRestaurant, String mail, String password) throws Exception {
        try {
            String role = getRole(mail, password);

            if (!role.equals(RoleEnum.ROLE_CLIENT.toString())) {
                throw new Exception("THE USER IS NOT A CUSTOMER PLEASE TRY AGAIN");
            }

            return dishRepository.getDishByCategory(IdRestaurant);
        } catch (Exception e) {
            throw new Exception("An error occurred while fetching dish by category: " + e.getMessage());
        }
    }
}
