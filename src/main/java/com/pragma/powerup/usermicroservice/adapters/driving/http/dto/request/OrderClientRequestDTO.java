package com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class OrderClientRequestDTO {
    private int idPlato;
    private String nombre;
    private int cantidad;
}
