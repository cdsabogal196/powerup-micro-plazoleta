package com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;
@AllArgsConstructor
@Getter
@Setter
public class OrderRequestDTO {
    private Long idCliente;
    private Date fecha;
    private String estado;
    private Long idChef;
    private Long idRestaurante;
    private List<OrderClientRequestDTO> platos;

}
