package com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.impl;

import com.nimbusds.jose.shaded.gson.Gson;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.RestaurantEntity;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.RestaurantRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.JwtResponseDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.RestaurantResponseDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.IRestaurantHandler;

import com.pragma.powerup.usermicroservice.domain.model.RoleEnum;
import com.pragma.powerup.usermicroservice.domain.repositories.IRestaurantRepository;
import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

@Service
@RequiredArgsConstructor
public class RestaurantHandlerImpl implements IRestaurantHandler {

    @Autowired
    private final IRestaurantRepository restaurantRepository;
    private final HttpClient client = HttpClient.newHttpClient();

    public String getToken(String mail, String password) {
        String requestBody = "{\"mail\":\"" + mail + "\", \"password\":\"" + password + "\"}";
        String endpoint = "http://localhost:8080/auth/login";
        String responseBody = sendHttpRequest(requestBody, endpoint);

        Gson gson = new Gson();
        JwtResponseDto responseObject = gson.fromJson(responseBody, JwtResponseDto.class);

        return responseObject.getToken();
    }

    private String sendHttpRequest(String requestBody, String endpoint) {
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(endpoint))
                .header("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(requestBody))
                .build();

        HttpResponse<String> response;
        try {
            response = client.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }

        return response.body();
    }

    public String getRole(String mail, String password) {
        String token = getToken(mail, password);

        String[] chunks = token.split("\\.");
        Base64.Decoder decoder = Base64.getUrlDecoder();

        String payload = new String(decoder.decode(chunks[1]));
        Gson gson = new Gson();
        JwtResponseDto jwtResponseDto = gson.fromJson(payload, JwtResponseDto.class);

        return jwtResponseDto.getRoles().get(0);
    }

    public void saveRestaurant(String mail, String password, RestaurantRequestDto restaurantRequestDto) throws Exception {
        HttpClient client = HttpClient.newHttpClient();

        String role = getRole(mail, password);
        String token = getToken(mail, password);
        if (role.equals(RoleEnum.ROLE_ADMIN.toString())) {
            HttpRequest requestOwner = HttpRequest.newBuilder()
                    .uri(URI.create("http://localhost:8080/user/" + restaurantRequestDto.getIdPropietario()))
                    .header("Authorization", "Bearer " + token)
                    .GET()
                    .build();
            HttpResponse<String> ownerResponse = client.send(requestOwner, HttpResponse.BodyHandlers.ofString());
            String ownerResponseBody = ownerResponse.body();

            JSONObject jsonObject = new JSONObject(ownerResponseBody);

            if (ownerResponseBody.contains("error")) {
                throw new Exception("The user doesn't exist");
            }

            String description = jsonObject.getJSONObject("role").getString("description");

            if (description.equals(RoleEnum.ROLE_OWNER.toString())) {
                RestaurantEntity restaurantEntity = new RestaurantEntity();
                restaurantEntity.setName(restaurantRequestDto.getName());
                restaurantEntity.setNit(restaurantRequestDto.getNit());
                restaurantEntity.setAddress(restaurantRequestDto.getAddress());
                restaurantEntity.setPhone(restaurantRequestDto.getPhone());
                restaurantEntity.setUrlLogo(restaurantRequestDto.getUrlLogo());
                restaurantEntity.setIdPropietario(restaurantRequestDto.getIdPropietario());

                restaurantRepository.save(restaurantEntity);
            } else {
                throw new Exception("The user to assign the restaurant is not an owner");
            }
        } else {
            throw new Exception("This user doesn't have authorization to create a restaurant");
        }
    }


    public List<RestaurantResponseDto> getRestaurantData(String mail, String password) throws Exception {
        String role = getRole(mail, password);
        List<RestaurantEntity> restaurants = restaurantRepository.getRestaurantData();
        List<RestaurantResponseDto> restaurantResponseDtos = new ArrayList<>();

        if (role.equals(RoleEnum.ROLE_CLIENT.toString())) {
            for (RestaurantEntity restaurantRequest : restaurants) {
                RestaurantResponseDto restaurantResponse = new RestaurantResponseDto();
                restaurantResponse.setName(restaurantRequest.getName());
                restaurantResponse.setUrlLogo(restaurantRequest.getUrlLogo());
                restaurantResponseDtos.add(restaurantResponse);
            }
        } else {
            throw new Exception("Role is not allowed");
        }

        return restaurantResponseDtos;
    }


}
