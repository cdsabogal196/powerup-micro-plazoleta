package com.pragma.powerup.usermicroservice.adapters.driving.http.handlers;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.DishEntity;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.DishRequestDTO;

import java.util.List;

public interface IDishHandler {
    void saveDish(String mail, String password ,DishRequestDTO dishRequestDto) throws Exception;

    void updateDish(Long id, String mail, String password, int price, String description);
    void activateDish(Long id, String mail, String password);

    List<DishEntity> getDishByCategory(String IdRestaurant, String mail, String password) throws Exception;
}
