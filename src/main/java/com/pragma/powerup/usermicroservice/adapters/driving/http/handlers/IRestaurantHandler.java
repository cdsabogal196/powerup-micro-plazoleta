package com.pragma.powerup.usermicroservice.adapters.driving.http.handlers;

import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.RestaurantRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.RestaurantResponseDto;


import java.util.List;

public interface IRestaurantHandler {
    void saveRestaurant(String mail,String password, RestaurantRequestDto restaurantRequestDto) throws Exception;
    List<RestaurantResponseDto> getRestaurantData(String mail,String password)  throws Exception ;
}
