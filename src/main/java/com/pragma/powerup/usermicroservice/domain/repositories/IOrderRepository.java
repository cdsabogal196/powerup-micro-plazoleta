package com.pragma.powerup.usermicroservice.domain.repositories;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.OrderEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface IOrderRepository extends JpaRepository<OrderEntity, Long> {

  Page<OrderEntity> findByEstado(String estado, Pageable pageable);

  @Query("SELECT p.estado FROM OrderEntity p WHERE p.id = ?1")
  String getEstadoById(String idPedido);
  OrderEntity findById(long idPedido);

  @Query("SELECT p.ping FROM OrderEntity p WHERE p.id = ?1")
  String findPingById(Long idPedido);

  @Query("SELECT p.idCliente FROM OrderEntity p WHERE p.id = ?1")
  String findIdClienteById(Long idPedido);
  @Modifying
  @Query("UPDATE OrderEntity p SET p.estado = 'Entregado' WHERE p.id = ?1")
  void markOrderDelivered(Long idPedido);

  @Modifying
  @Query("UPDATE OrderEntity p SET p.estado = 'Listo' WHERE p.id = ?1")
  void notifyOrderReady(Long idPedido);
  @Modifying
  @Query("UPDATE OrderEntity p SET p.estado = 'Cancelado' WHERE p.id = ?1")
  void cancelOrder(Long idPedido);
  @Modifying
  @Query("UPDATE OrderEntity p SET p.estado = 'Preparacion' WHERE p.id = ?1")
  void changeOrderPreparation(Long idPedido);
}
