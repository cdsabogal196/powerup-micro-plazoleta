package com.pragma.powerup.usermicroservice.domain.repositories;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.DishEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface IDishRepository extends JpaRepository<DishEntity, Long> {
    DishEntity findById(String id);

    @Query("SELECT p FROM DishEntity p WHERE p.restaurante.id = :restaurantId " +
            "AND p.categoria IN (SELECT DISTINCT p2.categoria FROM DishEntity p2 WHERE p2.restaurante.id = :restaurantId)")
    List<DishEntity> getDishByCategory(@Param("restaurantId") String restauranteId);

}