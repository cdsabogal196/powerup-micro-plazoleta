package com.pragma.powerup.usermicroservice.domain.model;

public enum OrderStateEnum {
    Pendiente,Preparacion,Listo,Entregado,Cancelado
}
