package com.pragma.powerup.usermicroservice.domain.model;

public enum RoleEnum
{
    ROLE_ADMIN, ROLE_USER, ROLE_OWNER, ROLE_EMPLOYEE, ROLE_CLIENT
}
