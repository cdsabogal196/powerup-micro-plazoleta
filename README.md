<br />
<div align="center">
<h3 align="center">PRAGMA POWER-UP</h3>
  <p align="center">
    In this challenge you are going to design the backend of a system that centralizes the services and orders of a restaurant chain that has different branches in the city.
  </p>
</div>

### Built With

* ![Java](https://img.shields.io/badge/java-%23ED8B00.svg?style=for-the-badge&logo=java&logoColor=white)
* ![Spring](https://img.shields.io/badge/Spring-6DB33F?style=for-the-badge&logo=spring&logoColor=white)
* ![Gradle](https://img.shields.io/badge/Gradle-02303A.svg?style=for-the-badge&logo=Gradle&logoColor=white)
* ![MySQL](https://img.shields.io/badge/MySQL-00000F?style=for-the-badge&logo=mysql&logoColor=white)


<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these steps.

### Prerequisites

* JDK 17 [https://jdk.java.net/java-se-ri/17](https://jdk.java.net/java-se-ri/17)
* Gradle [https://gradle.org/install/](https://gradle.org/install/)
* MySQL [https://dev.mysql.com/downloads/installer/](https://dev.mysql.com/downloads/installer/)

### Recommended Tools
* IntelliJ Community [https://www.jetbrains.com/idea/download/](https://www.jetbrains.com/idea/download/)
* Postman [https://www.postman.com/downloads/](https://www.postman.com/downloads/)

### Installation

1. Clone the repository
2. Change directory
   ```sh
   cd power-up-arquetipo-v3
   ```
3. Create a new database in MySQL called powerup
4. Update the database connection settings
   ```yml
   # src/main/resources/application-dev.yml
   spring:
      datasource:
          url: jdbc:mysql://localhost/powerup
          username: root
          password: <your-password>
   ```
5. After the tables are created execute src/main/resources/data.sql content to populate the database
6. Open Swagger UI and search the /auth/login endpoint and login with userDni: 123, password: 1234

<!-- USAGE -->
## Usage

1. Right-click the class PowerUpApplication and choose Run
2. Open [http://localhost:8090/swagger-ui/index.html](http://localhost:8090/swagger-ui/index.html) in your web browser

## Tests

- Right-click the test folder and choose Run tests with coverage
### USE ENPOINT /USER/

This is the README file for the project. Below, you will find instructions on how to use the API and access the endpoints using Swagger.

## Step 1: Accessing the API

Once you are in the environment where the API is located, you can access it using the corresponding URL.

## Step 2: Accessing Swagger

To interact with the API, go to the Swagger link. From there, you will be able to see all the available endpoints and test them.

http://localhost:8090/swagger-ui/index.html


## Step 2: Accessing microservicio

Descargar ambos microservicio  plazoleta y automaticamente tambien este "https://gitlab.com/cdsabogal196/powerup-micro-users" 


En este primero se realizara la generacion de token la creacion de usuarios y el tipo de usuario que permitira ejecutar el microservicio

SI ES 
1: ROLE_USER
2:ROLE_ADMIN
3:ROLE_OWNER

PARA ESTO ES NECESARIO CREAR LA BASE DE datos en mysqlWorkbench users y plazoleta

## Step 3: Choosing the /restaurant/ endpoint

In Swagger, look for the endpoint named `/restaurant/` and select it.

Este proyecto permite la creación de un restaurant pero el restaurante solo puede ser creado por el dueño por lo que se debe usar los campos mail y password en el sistema para validar que el que esta creando el restaurante es administador

```json
Copy code
{
  "mail": "correo_ejemplo@example.com",
  "password": "contraseña_secreta",
  
 ```
 Con estos obtendremos el rol y el token que nos dara que id tiene:

 To define an admin role (ROLE_ADMIN), use "id": 1.
To define a user role (ROLE_USER), use "id": 2.
To define a user role (ROLE_OWNER), use "id": 3.

para crear rol es owner crear un restaurante llenamos los siguientes campos
```json
Copy code
{
  "mail": "correo_ejemplo@example.com",
  "password": "contraseña_secreta",
  "name": "Nombre del usuario",
  "nit": "1234567890",
  "address": "Dirección del usuario",
  "phone": "123456789",
  "urlLogo": "https://ejemplo.com/logo.png",
  "idPropietario": 0
```

pero en el campo idPropietario asignaremos un id de un usuario del primer microservicio si es OWNER creara un restaurante de caso contrario no lo permitira.

```json

  "idPropietario": 3
```

### Step 4: Choosing the /plato/ endpoint

In Swagger, look for the endpoint named `/restaurant/` and select it.

Este endpoint plato realiza la misma accion obteniendo del microservicio user el token de donde obtendremos el id a partir del indgreso del mail y el password en el requestbody junto con la informacion dada

```json
Copy code
{
  "mail": "correo_ejemplo@example.com",
  "password": "contraseña_secreta",
  }
 ```
With these we will obtain the role and the token that will give us what id it has:

To define an admin role (ROLE_ADMIN), use "id": 1.
To define a user role (ROLE_USER), use "id": 2.
To define a user role (ROLE_OWNER), use "id": 3.

if is ROLE_ADMIN
```json
{
  "mail": "string",
  "password": "string",
  "nombre": "string",
  "precio": 0,
  "descripcion": "string",
  "urlImagen": "string",
  "categoria": "string",
  "idRestaurante":1
```
This will create the dish
### Step 5:Get Dishes by Category /platos/{restauranteId}/categoria

The "Get Dishes by Category" service allows you to obtain a list of dishes from a specific restaurant filtered by category. To access the service, it is required to provide the restaurantId, as well as the mail and password authentication credentials.

The service consists of two main components:

- Controller (Controller): This component is responsible for receiving HTTP requests and managing responses. Contains the getPlatesByCategoria method that implements the logic to get the dishes by category.

- Plato Handler: This component is in charge of the interaction with the data access layer (for example, a database) to obtain the dishes by category.
###USE

The "Get Dishes by Category" service allows you to obtain a list of dishes from a restaurant filtered by category. The service is accessed through an HTTP GET request and requires the following parameters in the URL:

- restaurantId: the unique identifier of the restaurant from which you want to obtain the dishes.
- mail: the user's email for authentication.
- password: the user's password for authentication.

### Step 5: Realize Order /PEDIDO
The "Realize Order" service allows you to create a new order. This service is accessed through an HTTP POST request. You need to provide the PedidoRequestDTO object in the request body, as well as the mail and password authentication credentials.

Controller
The Controller component contains the realizarPedido method, which implements the logic to create a new order. It receives the PedidoRequestDTO object, mail, and password as parameters.


```json
@PostMapping
public ResponseEntity<String> realizarPedido(@RequestBody PedidoRequestDTO pedidoDTO, String mail, String password) {
  
```

The realizarPedido method calls the pedidoHandler.realizarPedido method to perform the actual order creation process.
The realizarPedido method performs the following steps:

- Performs authentication by sending a request to the authentication service.
- Validates the user's role to ensure they have the necessary permissions.
- Creates a new PedidoEntity object and sets its properties based on the provided PedidoRequestDTO.
- Saves the new order in the pedidoRepository.
- Iterates over the list of dishes (platos) in the PedidoRequestDTO and creates PedidoPlatoEntity objects to associate the dishes with the order.
- Saves each PedidoPlatoEntity in the pedidoPlatoRepository.
#Response
The response for the "Realize Order" service is a String message indicating the status of the order creation. The response will be:
```
The order was created successfully and is in state Pending
```
This message confirms that the order was created successfully and is currently in the "Pending" state.

### Step 6: Change Order Status "/{idPedido}/estado"
The "Change Order Status" service allows you to update the status of an existing order. This service is accessed through an HTTP PUT request. You need to provide the idPedido parameter in the URL and the PedidoRequestDTO object in the request body. Additionally, the mail and password authentication credentials are required.

The Controller component contains the cambiarEstadoPedido method, which implements the logic to change the status of an order. It receives the idPedido, PedidoRequestDTO, mail, and password as parameters.

```json

@PutMapping("/{idPedido}/estado")
public ResponseEntity<String> cambiarEstadoPedido(@PathVariable int idPedido, @RequestBody PedidoRequestDTO estadoPedidoDTO, String mail, String password) {
  
} 

```
The cambiarEstadoPedido method calls the pedidoHandler.cambiarEstadoPedido method to perform the actual status change process. 
If an exception occurs, a response with status 500 (Internal Server Error) and an error message is returned. If the status change is successful, a response with status 200 (OK) and a success message is returned. 
If the status of the order is ready, it can be changed from pending to prepared, or delivered.


### Step 7: Get Orders by State
The "Get Orders by State" service allows you to retrieve a list of orders based on their state. This service is accessed through an HTTP GET request. It requires the following parameters:

- estado: The state of the orders to retrieve.
- elementosPorPagina: The number of elements per page.
- numeroPagina: The page number.
- To access the service, you also need to provide the mail and password authentication credentials.

The Controller component contains the obtenerPedidosPorEstado method, which implements the logic to retrieve orders by state. It receives the estado, elementosPorPagina, numeroPagina, mail, and password as parameters.

```json

@GetMapping
public ResponseEntity<Page<PedidoEntity>> obtenerPedidosPorEstado(
        @RequestParam("estado") String estado,
        @RequestParam("elementosPorPagina") int elementosPorPagina,
        @RequestParam("numeroPagina") int numeroPagina,
        String mail,
        String password) throws Exception {
    // Authentication process
    // ...

    Page<PedidoEntity> pedidos = pedidoHandler.obtenerPedidosPorEstado(estado, elementosPorPagina, numeroPagina, mail, password);
    return ResponseEntity.ok(pedidos);
}


```
The obtenerPedidosPorEstado method performs the following steps:

- Receives the request parameters: estado, elementosPorPagina, numeroPagina, mail, and password.
- Performs authentication by sending a request to the authentication service.
- Validates the user's role to ensure they have the necessary permissions.
- Calls the obtenerPedidosPorEstado method in the pedidoHandler component to retrieve the orders based on the provided parameters.
- Returns a ResponseEntity containing the retrieved orders as a Page<PedidoEntity>.
Pageable
- The elementosPorPagina and numeroPagina parameters are used to implement pagination in the "Get Orders by State" service. Pagination allows you to retrieve orders in chunks or pages, rather than fetching all orders at once. This can improve performance and reduce the amount of data transferred over the network.

- elementosPorPagina: Specifies the number of orders to include per page. For example, if elementosPorPagina is set to 10, each page will contain up to 10 orders.
- numeroPagina: Specifies the page number to retrieve. The first page is typically numbered as 0 or 1, depending on the implementation.

The Pageable object in the response provides information about the pagination settings. It includes the current page number, the number of elements per page, the total number of elements, and the total number of pages.


```json
"pageable": {
  "sort": {
    "sorted": false,
    "unsorted": true,
    "empty": true
  },
  "pageNumber": 0,
  "pageSize": 10,
  "offset": 0,
  "paged": true,
  "unpaged": false
},
"totalElements": 20,
"totalPages": 2


```
- sort: Indicates whether the results are sorted.
- pageNumber: The current page number.
- pageSize: The number of elements per page.
- offset: The offset of the current page.
- paged: Indicates whether the results are paged.
- unpaged: Indicates whether the results are unpaged.
- totalElements:total elements of the page

### Step 11: User Story: List the dishes of a restaurant

As a customer of the food court, I need to be able to list the menu of each restaurant that I click on in order to request the dish of my choice.

## Requirements:

The dishes of each menu should be listed, grouped by category.
The dishes must be displayed in a paginated way, allowing you to specify the desired number of elements per page.
endpoints
Get dishes from a restaurant by category
```
URL: /plato/platos/{restauranteId}/categoria
```

## Method: GET

URL parameters:

restaurantId (required): ID of the restaurant from which you want to obtain the dishes.
Query parameters:

  1. mail (required): Customer email.
  2. password (required): Customer password.
Request example:
```
"GET /plato/platos/123/categoria?mail=cliente@example.com&password=secreto"
```

```json
[
  {
    "categoria": "Entradas",
    "platos": [
      {
        "id": 1,
        "nombre": "Ensalada César",
        "precio": 10.99
      },
      {
        "id": 2,
        "nombre": "Carpaccio de res",
        "precio": 12.99
      }
    ]
  },
  {
    "categoria": "Platos Principales",
    "platos": [
      {
        "id": 3,
        "nombre": "Lomo saltado",
        "precio": 15.99
      },
      {
        "id": 4,
        "nombre": "Ceviche mixto",
        "precio": 14.99
      }
    ]
  }
```
Remember to provide the email and password to authenticate yourself and get access to the list of dishes of the desired restaurant.

Make sure to properly configure the microservice and the corresponding databases for the correct operation of the user story.

Remember to adapt the information and instructions according to the specific needs of your project.

### Step 11 Food Court - Order Microservice

This microservice is responsible for managing orders placed by customers at the food court. It allows customers to place and query orders, as well as obtain information about the dishes available at each restaurant.

## User Story: Place an Order
As a customer at the food court, I want to be able to order dishes of my preference so that they can be prepared and brought to my table.

## Requirements:
1. An order consists of a list of dishes from a single restaurant.
2. Every order must specify the restaurant, the chosen dishes, and the quantity of each dish.
3. Immediately after placing an order, it should be set to the "Pendiente" state.
4. A customer can place a new order only if they don't have any orders in progress (preparation, pending, or ready).
# Endpoints
"URL: /plato/save"

# Method: POST
# Query Parameters:

1. mail (required): Customer's email address.
2. password (required): Customer's password.

```json

{
  "nombre": "string",
  "precio": 0,
  "descripcion": "string",
  "urlImagen": "string",
  "categoria": "string",
  "activo": true,
  "idRestaurante": 0
```
solicitud example

```
- POST /plato/save?mail=cliente@example.com&password=secreto
```
```json

{
  "nombre": "Lomo Saltado",
  "precio": 15.99,
  "descripcion": "Delicioso lomo saltado con ingredientes frescos",
  "urlImagen": "https://example.com/images/lomo-saltado.jpg",
  "categoria": "Platos Principales",
  "activo": true,
  "idRestaurante": 123
```

Successful answer:

```json
{
  "id": 1,
  "nombre": "Lomo Saltado",
  "precio": 15.99,
  "descripcion": "Delicioso lomo saltado con ingredientes frescos",
  "urlImagen": "https://example.com/images/lomo-saltado.jpg",
  "categoria": "Platos Principales",
  "activo": true,
  "idRestaurante": 123
```
Make sure to provide the email address and password to authenticate and place the order successfully.


### hu 13 -change state Order Management
This microservice handles the management of orders placed by customers at the food court. It provides functionalities for customers to place orders, as well as for employees to update the status of orders.

# User Story: Mark Order as Delivered
As an employee of a restaurant, I want to be able to mark an order as delivered so that I can complete the order cycle and focus on other orders.

# Requirements:

- Only orders in the "Ready" state can be marked as delivered.
- Once an order is marked as delivered, it cannot be changed to any state other than "Ready".
- To change the status to "Delivered", the employee must enter the security PIN that was sent to the customer for order collection.
# Endpoints
Mark Order as Delivered
URL: /pedido/cambiarEstadoPreparacion

# Method: PUT

Query Parameters:

1. idPedido (required): Order ID.
2. mail (required): Employee's email address.
3. password (required): Employee's password.
4. idEmpleado (required): Employee ID.
Example request:
```
PUT /pedido/cambiarEstadoPreparacion?idPedido=123&mail=employee@example.com&password=secret&idEmpleado=456
```
Successful response:
makefile
Copy code
Status: 200 OK
Make sure to provide the order ID, email address, password, and employee ID to authenticate and mark the order as delivered.


### Food Court - Order Management
This repository contains the code for the Order Management microservice of the Food Court application. The microservice allows customers to place orders and provides functionalities for managing the status of orders.

# User Story: Cancel Order
As a customer of a restaurant, I want to be able to cancel my order in case I change my mind or have any other reason to retract.

# Requirements
1. Only orders in the "Pending" state can be canceled.
2. If the order is in a state other than "Pending", the user should be notified with the message:   "We're sorry, your order is already being prepared and cannot be canceled."
API Endpoint
Cancel Order

bash
```
POST /pedido/{idPedido}/cancelar
```
This endpoint allows the customer to cancel their order.

Path Parameters:

1. idPedido (required): The ID of the order to be canceled.

2. mail (required): The customer's email address.
3. password (required): The customer's password.
Example Request:
```json
POST /pedido/123/cancelar?mail=customer@example.com&password=secret
```
Successful Response:

Status: 200 OK
Make sure to provide the correct order ID, customer's email address, and password to authenticate and cancel the order.

### User Story 15: Employee - Mark Order as Delivered
As an employee of a restaurant, I need to mark an order as delivered in order to complete its cycle and focus on other orders.

## Acceptance Criteria
1. Only orders in the "Ready" state can be marked as delivered.
2. No order in the "Delivered" state can be modified to change its state to anything other than "Ready".
3. To change the state to "Delivered", the employee must enter the security PIN that was sent to the customer to claim their order.
4. The employee's role is determined by providing the email and password as query parameters.
```
POST /pedido/{idPedido}/entregar/{pinSeguridad}
```

## Parameters
1. idPedido: (Path parameter) The ID of the order to be marked as delivered.
2. pinSeguridad: (Path parameter) The security PIN provided to the customer to claim their order.
3. mail: (Query parameter) The email of the employee. (Used for role verification)
4. password: (Query parameter) The password of the employee. (Used for role verification)
5. idEmpleado: (Query parameter) The ID of the employee.


```json
POST /pedido/12345/entregar/6789?mail=employee@example.com&password=employee123&idEmpleado=5678
```
The response will depend on the implementation of the API. It can include a success message indicating that the order has been successfully marked as delivered or an error message if the operation fails.

Please note that the email and password are used for role verification, and the security PIN is used to confirm the order delivery by the employee.


